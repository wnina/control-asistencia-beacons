package com.example.veox.control_asitencia_beacons;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.zip.Inflater;

import model.Course;

/**
 * Created by student on 5/11/17.
 */

public class CustomAdapterCourse extends ArrayAdapter<Course> {

    private Context context;
    private ArrayList<Course> listCourses;
    // private int p;
    public CustomAdapterCourse(Context context, ArrayList<Course> listCourses) {
        super(context, 0, listCourses);
        this.context = context;
        this.listCourses = listCourses;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.row, parent, false);

        final ImageView ivPhoto  = (ImageView)convertView.findViewById(R.id.ivPhoto);
        TextView tvTitle = (TextView)convertView.findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView)convertView.findViewById(R.id.tvDescription);
        TextView tvOpening = (TextView) convertView.findViewById(R.id.tvOpening);

        ivPhoto.setImageResource(listCourses.get(position).getImage());
        tvTitle.setText(listCourses.get(position).getName());
        tvDescription.setText(listCourses.get(position).getDescription());
        tvOpening.setText(listCourses.get(position).getOpening().toString());

        // p = position;
        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("clic-ivPhoto", "Estoy haciendo para ir a sesion");
                Intent i = new Intent(v.getContext(), SessionsActivity.class);
                i.putExtra("nameCourse",  listCourses.get(position).getName());
                context.startActivity(i);
            }
        });

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Dialog d = new Dialog(v.getContext());
                d.setContentView(R.layout.dialog);
                d.show();
                d.findViewById(R.id.bCancelD).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                    }
                });
                d.findViewById(R.id.bAcceptD).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseReference dbr = FirebaseDatabase.getInstance().getReference();
                        dbr.child("courses").child(listCourses.get(position).getHash()).removeValue();
                        d.dismiss();
                    }
                });
                return false;
            }
        });

        return convertView;
    }
}
