package com.example.veox.control_asitencia_beacons;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import database.MyDatabase;
import database.PersonEntity;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.etUser)
    EditText etUser;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPassword)
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        final Context c = this;
        ButterKnife.bind(this);


        Button bRegister  = (Button)findViewById(R.id.bRegisterTwo);
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, DashboardAdminActivity.class);
                startActivity(i);
                //Recuperar datos de la interfaz
                final String user = etUser.getText().toString();
                final String email = etEmail.getText().toString();
                final String password = etPassword.getText().toString();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //Guardar en la base de datos
                        MyDatabase database = MyDatabase.getInstance(null);
                        database.personDao().insert(new PersonEntity(user, 0, email, password));
                        List<PersonEntity> lists = database.personDao().getAll();
                    }
                }).start();

            }
        });
    }
}
