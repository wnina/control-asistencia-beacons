package com.example.veox.control_asitencia_beacons;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class DashboardAdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_admin);

        final Context c = this;
        ImageButton ibCourses = (ImageButton)findViewById(R.id.ibCoursesAdmin);
        ibCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, CoursesAdminActivity.class);
                startActivity(i);
            }
        });

    }
}
