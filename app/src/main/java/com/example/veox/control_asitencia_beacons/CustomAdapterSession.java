package com.example.veox.control_asitencia_beacons;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import model.Session;

/**
 * Created by student on 11/11/17.
 */

public class CustomAdapterSession extends ArrayAdapter<Session> {

    ArrayList<Session> listSessions;
    Context context;

    public CustomAdapterSession(Context c, ArrayList<Session> listSessions){
      super(c, 0, listSessions);
        this.context = c;
        this.listSessions = listSessions;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.row_session, parent, false);
        TextView tvSession = (TextView) convertView.findViewById(R.id.tvSession);
        TextView tvState = (TextView) convertView.findViewById(R.id.tvState);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tvDate);
        TextView tvCheckIn = (TextView) convertView.findViewById(R.id.tvCheckIn);
        TextView tvCheckInReal = (TextView) convertView.findViewById(R.id.tvCheckInReal);
        TextView tvCheckOut = (TextView) convertView.findViewById(R.id.tvCheckOut);
        TextView tvCheckOutReal = (TextView) convertView.findViewById(R.id.tvCheckOutReal);

        tvSession.setText(listSessions.get(position).getName());
        tvState.setText(String.valueOf(listSessions.get(position).getState()));
        tvDate.setText(listSessions.get(position).getDate().toString());
        tvCheckIn.setText(listSessions.get(position).getCheck_in().toString());
        tvCheckInReal.setText("00:00");
        tvCheckOut.setText(listSessions.get(position).getCheck_out().toString());
        tvCheckOutReal.setText("00:00");

        return convertView;
    }
}
