package com.example.veox.control_asitencia_beacons;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        final Context c = this;
        ImageButton ibCourses = (ImageButton)findViewById(R.id.ibCourses);
        ibCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, CoursesActivity.class);
                startActivity(i);
            }
        });

    }
}
