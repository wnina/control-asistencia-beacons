package com.example.veox.control_asitencia_beacons;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import database.CourseEntity;
import database.CoursePersonEntity;
import database.CourseSessionEntity;
import database.MyDatabase;
import database.SessionEntity;
import model.Course;

public class CoursesAdminActivity extends AppCompatActivity {

    private ArrayList<Course> listCourses;
    private DatabaseReference db;
    CustomAdapterCourse customAdapterCourse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses_admin);
        final Context c = this;
        db = FirebaseDatabase.getInstance().getReference();

        Button bAddCourse = findViewById(R.id.bAddCourse);
        bAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new Dialog(c);
                d.setContentView(R.layout.dialog_add_course);
                d.show();
                d.findViewById(R.id.bAddCourseD).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText etCode =  d.findViewById(R.id.etCodeAdmin);
                        EditText etName =  d.findViewById(R.id.etNameAdmin);
                        EditText etDescription =  d.findViewById(R.id.etDescriptionAdmin);

                        DatabaseReference dbr =  db.child("courses").push();
                        dbr.child("code").setValue(etCode.getText().toString());
                        dbr.child("name").setValue(etName.getText().toString());
                        dbr.child("description").setValue(etDescription.getText().toString());
                        d.dismiss();
                    }
                });
                d.findViewById(R.id.bCancelCourseD).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                    }
                });
            }
        });

        ListView lvCourse = (ListView)findViewById(R.id.lvCourses);
        listCourses = new ArrayList<>();
        customAdapterCourse = new CustomAdapterCourse(c, listCourses);
        lvCourse.setAdapter(customAdapterCourse);
        readDataFirebase();

    }

    public void readDataFirebase(){
        db.child("courses").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listCourses.clear();
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    Object code = data.child("code").getValue();
                    Object name = data.child("name").getValue();
                    Object description = data.child("description").getValue();
                    if(code != null && name != null && description != null){
                        listCourses.add(new Course(data.getKey(), Integer.valueOf(code.toString()),
                                new Date("10/10/10"), name.toString(), description.toString(), 0, null, null, null));
                        customAdapterCourse.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



}
