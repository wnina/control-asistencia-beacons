package model;

import java.util.ArrayList;
import java.util.List;


public class Teacher extends Person{
  
  public Teacher(String name, int dni, String address, String email){
    super(name, dni, address, email); 
  }
  
  public boolean doManualAssistance(Course couse){
     return true;
  }
  
  
  public List<Assistance> getAssistances(){
    return null;
  }

  public List<Assistance> getAssistancesByPerson(int dni){
    return null;    
  }

  public List<Assistance> getAssistancesByCourse(int code){
    return null;
  }
  
}