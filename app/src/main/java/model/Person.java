package model;

import java.util.List;
import java.util.ArrayList;

public abstract class Person{
  
  private String name;
  private int dni;
  private String address;
  private String email;
  
  public Person(String name, int dni, String address, String email){
    this.name = name;
    this.dni = dni;
    this.address = address;
    this.email = email;
  }
  
  public boolean login(){
    return true;
  }
  
  public boolean register(){
    return true;
  }

  public abstract List<Assistance> getAssistances();
  
  public abstract List<Assistance> getAssistancesByCourse(int code);
  
  
}