package model;

import java.util.Date;

public class Assistance{
  
  private Date check_in;
  private Date check_out;
  private Person person;
  
  public Assistance(Date check_in, Date check_out, Person person){
    this.check_in = check_in;
    this.check_out = check_out;
    this.person = person;
  }
  
}