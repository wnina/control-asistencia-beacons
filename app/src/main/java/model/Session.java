package model;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class Session{

  private String name;
  private Date date;
  private Date check_in;
  private Date check_out;
  private List<Assistance>  listAssistance;
  private int state; // 0: abierto, 1: finalizado, 2: no finalizado
  
  public Session(String name, Date date, Date check_in, Date check_out){
    this.name = name;
    this.date = date;
    this.check_in = check_in;
    this.check_out = check_out;
    this.listAssistance = new ArrayList<Assistance>();
    this.state = 0;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Date getCheck_in() {
    return check_in;
  }

  public void setCheck_in(Date check_in) {
    this.check_in = check_in;
  }

  public Date getCheck_out() {
    return check_out;
  }

  public void setCheck_out(Date check_out) {
    this.check_out = check_out;
  }

  public List<Assistance> getListAssistance() {
    return listAssistance;
  }

  public void setListAssistance(List<Assistance> listAssistance) {
    this.listAssistance = listAssistance;
  }

  public int getState() {
    return state;
  }

  public void setState(int state) {
    this.state = state;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}