package model;

import java.util.ArrayList;
import java.util.List;

public class Student extends Person{
  
  public Student(String name, int dni, String address, String email){
    super(name, dni, address, email);  
  }
  
  public List<Assistance> getAssistances(){
    return null;
  }

  public List<Assistance> getAssistancesByCourse(int code){
    return null;
  }
  
}