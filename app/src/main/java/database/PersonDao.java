package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by student on 2/12/17.
 */

@Dao
public interface PersonDao {

    @Insert
    void insert(PersonEntity personEntity);

    @Query("SELECT * FROM PersonEntity")
    List<PersonEntity> getAll();

}
