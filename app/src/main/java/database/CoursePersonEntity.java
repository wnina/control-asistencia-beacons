package database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import model.Course;
import model.Person;

/**
 * Created by student on 2/12/17.
 */
@Entity(foreignKeys = {
        @ForeignKey(entity = CourseEntity.class, childColumns = "id_course", parentColumns = "id"),
        @ForeignKey(entity = PersonEntity.class, childColumns = "id_person", parentColumns = "id")
        }
        )
public class CoursePersonEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int id_course;
    private int id_person;

    public CoursePersonEntity(int id_course, int id_person) {
        this.id_course = id_course;
        this.id_person = id_person;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_course() {
        return id_course;
    }

    public void setId_course(int id_course) {
        this.id_course = id_course;
    }

    public int getId_person() {
        return id_person;
    }

    public void setId_person(int id_person) {
        this.id_person = id_person;
    }
}
