package database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by student on 16/12/17.
 */

@Entity(foreignKeys = {
        @ForeignKey(entity = CourseEntity.class, childColumns = "id_course", parentColumns = "id"),
        @ForeignKey(entity = SessionEntity.class, childColumns = "id_session", parentColumns = "id")
    }
)
public class CourseSessionEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int id_course;
    private int id_session;


    public CourseSessionEntity(int id_course, int id_session) {
        this.id_course = id_course;
        this.id_session = id_session;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_course() {
        return id_course;
    }

    public void setId_course(int id_course) {
        this.id_course = id_course;
    }

    public int getId_session() {
        return id_session;
    }

    public void setId_session(int id_session) {
        this.id_session = id_session;
    }
}
