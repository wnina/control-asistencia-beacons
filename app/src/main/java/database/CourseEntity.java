package database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by student on 26/11/17.
 */

@Entity
public class CourseEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int code;
    private String opening;
    private String name;
    private String description;
    private int id_iname;

    public CourseEntity(int code, String opening, String name, String description, int id_iname) {
        this.code = code;
        this.opening = opening;
        this.name = name;
        this.description = description;
        this.id_iname = id_iname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getOpening() {
        return opening;
    }

    public void setOpening(String opening) {
        this.opening = opening;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId_iname() {
        return id_iname;
    }

    public void setId_iname(int id_iname) {
        this.id_iname = id_iname;
    }
}
