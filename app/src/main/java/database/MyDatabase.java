package database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by student on 26/11/17.
 */

@Database(entities = {CourseEntity.class, PersonEntity.class, CoursePersonEntity.class, SessionEntity.class, CourseSessionEntity.class}, version = 1)
public abstract  class MyDatabase extends RoomDatabase {

    private  static MyDatabase database;

    public abstract CourseDao courseDao();
    public abstract PersonDao personDao();
    public abstract CoursePersonDao coursePersonDao();
    public abstract SessionDao sessionDao();
    public abstract CourseSessionDao courseSessionDao();

    public static  MyDatabase getInstance(Context c){
        if( database == null){
            database = Room.databaseBuilder(c, MyDatabase.class, "db-ca").build();
        }
        return database;
    }

}
