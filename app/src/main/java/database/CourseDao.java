package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by student on 26/11/17.
 */

@Dao
public interface CourseDao {

    @Query("SELECT * FROM CourseEntity")
    List<CourseEntity> getAll();

    @Insert
    void insert(CourseEntity course);

    @Insert
    void insertAll(List<CourseEntity> listCourses);

    @Query("SELECT * FROM CourseEntity WHERE name = :name")
    List<CourseEntity> getCourseByName(String name);

    @Query("SELECT * FROM CourseEntity WHERE name LIKE :name AND description LIKE :description")
    List<CourseEntity> getCourseByNameAndDescription(String name, String description);

}
