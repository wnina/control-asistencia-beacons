package database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


/**
 * Created by student on 3/12/17.
 */

@Entity
public class SessionEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String date;
    private String check_in;
    private String check_out;
    private int state; // 0: abierto, 1: finalizado, 2: no finalizado

    public SessionEntity(String name, String date, String check_in, String check_out, int state) {
        this.name = name;
        this.date = date;
        this.check_in = check_in;
        this.check_out = check_out;
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCheck_in() {
        return check_in;
    }

    public void setCheck_in(String check_in) {
        this.check_in = check_in;
    }

    public String getCheck_out() {
        return check_out;
    }

    public void setCheck_out(String check_out) {
        this.check_out = check_out;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
